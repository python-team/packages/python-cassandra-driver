python-cassandra-driver (3.29.2-5) unstable; urgency=medium

  * Increase difference range to pass test_nts_token_performance test on
    riscv64 (Closes: #1098277).
    - Thanks for patch Bo YU <vimer@debian.org>.

 -- Emmanuel Arias <eamanu@debian.org>  Tue, 18 Feb 2025 16:21:22 +0000

python-cassandra-driver (3.29.2-4) unstable; urgency=medium

  * Fix the build of cassandra.io.libevwrapper extension on Python 3.13

 -- Sophie Brun <sophie@offensive-security.com>  Thu, 13 Feb 2025 14:14:59 +0100

python-cassandra-driver (3.29.2-3) unstable; urgency=medium

  * Team Upload

  [ Arnaud Rebillout ]
  * Fix dh_auto_test syntax (Closes: #1084330)
  * Add patch to fix test when python version ends with a plus sign

 -- Sophie Brun <sophie@offensive-security.com>  Fri, 08 Nov 2024 16:02:33 +0100

python-cassandra-driver (3.29.2-2) unstable; urgency=medium

  * Team upload.
  * Fix: disable a test that fails on riscv64 (Closes: #1076838)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 16 Sep 2024 08:50:58 +0200

python-cassandra-driver (3.29.2-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 3.29.2
  * Replace Nose with Pytest
  * Refresh patches

 -- Alexandre Detiste <tchet@debian.org>  Fri, 13 Sep 2024 15:18:10 +0200

python-cassandra-driver (3.29.1-1.1) unstable; urgency=medium

  * Team Upload.
  * Remove extraneous dependency on python3-sure,
    still keep python3-nose for now.

 -- Alexandre Detiste <tchet@debian.org>  Sat, 25 May 2024 19:59:59 +0200

python-cassandra-driver (3.29.1-1) unstable; urgency=medium

  * New upstream version.
  * d/control: Bump Standards-Version to 5.7.0 (from 4.6.2; no further changes
    needed).

 -- Emmanuel Arias <eamanu@debian.org>  Mon, 22 Apr 2024 16:21:50 -0300

python-cassandra-driver (3.29.0-3) unstable; urgency=medium

  * d/rules: Ignore buildtime test failures on big endian. Thanks
    Adrian Bunk for the patch (Closes: #1064403).

 -- Emmanuel Arias <eamanu@debian.org>  Thu, 22 Feb 2024 13:41:56 -0300

python-cassandra-driver (3.29.0-2) unstable; urgency=medium

  * d/tests/control: Avoid run autopkgtests in s390x.

 -- Emmanuel Arias <eamanu@debian.org>  Thu, 22 Feb 2024 13:41:50 -0300

python-cassandra-driver (3.29.0-1) unstable; urgency=medium

  [ Emmanuel Arias ]
  * New upstream version.
  * d/control: Use python3-nose, instead of python3-nose2. Also changed the
    variable PYBUILD_TEST_NOSE=1 for PYBUILD_TEST_NOSE2=1 in rules.
  * d/control: Remove python3-six from Build Depends, it is not longer used.
  * d/patches/change-assertRaisesRegexp-in-test.patch: Add patch to replace
    the deprecated assertRaisesRegexp class method for assertRaisesRegex.
  * d/control: Add build dependency to python3-pyasyncore, the upstream code
    still use asyncore.
  * d/rules: remove tests/unit/cython/test_bytesio.py and
    tests/unit/cython/test_utils.py tests files. They fail for Python 3.12,
    need more investigation.
  * d/tests/control: Add python3-pyasyncore and python3-setuptools as Depends
    and use nose instead of nose2.

  [ Andreas Tille ]
  * Reorder sequence of d/control fields by cme (routine-update)
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)
  * Relax versioned Depends from cython
    Closes: #1056852

 -- Emmanuel Arias <eamanu@debian.org>  Sun, 28 Jan 2024 19:21:02 -0300

python-cassandra-driver (3.28.0-1) unstable; urgency=medium

  [ Colin Watson ]
  * Mark python3-cassandra-doc Multi-Arch: foreign.

  [ Emmanuel Arias ]
  * New upstream release.
  * d/control: Update my contact information. Also in d/copyright.
  * d/patches: Update patches:
    - 0004-Fix-typos-in-upstream-source.patch,
      0006-Merge-pull-request-1119-from-datastax-python-1290.patch and
      0007-Python-3.11-support-bpo-46730.patch: remove patches already applied
      by upstream.
    - 0003-Skip-tests-that-fails-on-i386-arch.patch
      0005-Skip-test_multi_timer_validation-flaky-test.patch and
      Make_unittests_loadable.patch: Update patches according to new upstream
      code. Skip tests manually, because nose2 does not support raising skip
      tests.
  * d/copyright: Update copyright year for debian/* files.
  * d/control: Bump Standards-Version to 4.6.2 (from 4.6.1; no further changes
    needed).
  * d/control: Run tests using nose2 instead of nose (Closes: #1018470).
    - d/rules: Set PYBUILD_TEST_NOSE2=1 to run nose2.
  * d/rules: Remove integration folder from tests. Remove tests that import
    integration: tests/stress_tests/test_multi_inserts.py,
    tests/stress_tests/test_load.py tests/unit/io/test_eventletreactor.py.

 -- Emmanuel Arias <eamanu@debian.org>  Sat, 07 Oct 2023 09:39:57 -0300

python-cassandra-driver (3.25.0-2) unstable; urgency=medium

  * Team upload.
  * Patch: Python 3.11 support. (Closes: #1024043)

 -- Stefano Rivera <stefanor@debian.org>  Wed, 23 Nov 2022 20:35:16 +0200

python-cassandra-driver (3.25.0-1) unstable; urgency=medium

  [ Nicolas Dandrimont ]
  * New upstream release

  [ Emmanuel Arias]
  * d/control: Bump Standards-Version to 4.6.1 (from 4.5.1; no further
    changes).
  * d/control: Remove trivial autopkgtest-pkg-python.
  * d/copyright: Update Copyright for year for Debian files.
  * d/upstream/metadata: Fix Name key in file. Use Python-Cassandra-Files
    instead of TileDB.
  * d/patches/0004-Fix-typos-in-upstream-source.patch: fix typos in upstream
    source.
  * d/tests: Run autopkgtest for this package.
  * d/patches/0005-Skip-test_multi_timer_validation-flaky-test.patch: Skip
    flaky test (Closes: #1000612).

 -- Emmanuel Arias <eamanu@yaerobi.com>  Fri, 13 May 2022 17:37:40 -0300

python-cassandra-driver (3.24.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Emmanuel Arias ]
  * d/control: Add Breaks+Replaces python3-cassandra (<< 3.20)
    (Closes: #952753).
  * d/gbp.conf: set debian/master as default branch.
  * d/control: Bump debhelper-compat to 13
  * d/control: Bump Standard-Version to 4.5.1
  * wrap-and-sort
  * d/control: No check the version of python3-mock dependency.
  * d/copyright: Update year copyright  on debian/*.
  * d/copyright: Use expat license name instead of MIT license name.
  * d/control: Update my contact information on Uploaders field.
  * New upstream version 3.24.0
  * d/control: Add python3-pure-sasl dependency.
  * d/patches: add patch to skip test that fail on i386 arch.

 -- Emmanuel Arias <eamanu@yaerobi.com>  Fri, 05 Feb 2021 13:27:33 -0300

python-cassandra-driver (3.20.2-2) unstable; urgency=medium

  * Team upload.
  * Rebuilt source-only after the package went through NEW.

 -- Thomas Goirand <zigo@debian.org>  Fri, 14 Feb 2020 09:08:44 +0100

python-cassandra-driver (3.20.2-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Adam Cecile ]
  * New upstream release 3.20.2.
  * Add several build-dependencies to run unit tests.
  * Enable unit tests.
  * Re-enable Cython which is working fine now.
  * Enable multi-core compilation of Cython files.
  * Switch to GitHub tarball instead of PyPi to have docs and unit tests.
  * Generate Sphinx doc in python3-cassandra-doc package.
  * Update debian/watch to track GitHub.
  * Bump Standards-Version to 4.5.0.
  * Add Rules-Requires-Root: no in control.
  * Add debian/upstream/metadata file.
  * Add myself to uploaders.

 -- Adam Cecile <acecile@le-vert.net>  Tue, 07 Jan 2020 12:43:32 +0100

python-cassandra-driver (3.16.0-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/changelog: Remove trailing whitespaces.
  * Drop Python 2 support.
  * Use pybuild for building package.
  * Add python3-six to build depends.
  * Enable autopkgtest-pkg-python testsuite.
  * Enable all hardening.
  * d/copyright: Fix cassandra/cmurmur3.c filename.
  * Bump standards version to 4.4.0 (no changes).
  * Bump debhelper compat level to 12.

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 12:49:01 +0200

python-cassandra-driver (3.16.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * d/changelog: Remove trailing whitespaces.

  [ Emmanuel Arias ]
  * New upstream release.
  * Add DPMT to Maintainer field on d/control.
  * Adopt package, add myself to Uploaders (Closes: #888400).
  * Update Standards Version from 4.1.4 to 4.2.1 on
    d/control file.
  * Update on d/copyright file the Copyright file for
    debian/* files.
    - Add me to there.
  * Bump debhelper compatibility to 11 (from 10).
  * Fix problems on d/copyright:
    - Fix dep5-copyright-license-name-not-unique tag lintian
  * Move from old -dbg packages to dbgsym (Closes: #857298).
    - Delete python-cassandra-dbg and python-cassandra[3]-dbg
      from d/control
    - Delete from override_dh_auto_install (on d/rules) the
      installation of *-dbg
    - Change existing -dbg to -dbgsym on d/rules

 -- Emmanuel Arias <emmanuelarias30@gmail.com>  Wed, 05 Dec 2018 21:16:10 +0200

python-cassandra-driver (3.14.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol

  [ Sandro Tosi ]
  * New upstream release
  * debian/rules
    - build for all supported python3 versions; patch by Scott Kitterman;
      Closes: #867010
    - remove cassandra/io/asyncioreactor.py from python2 package, code is meant
      to be py3k only
  * debian/copyright
    - extend packaging copyright years
    - update upstream copyright years
  * debian/control
    - bump Standards-Version to 4.1.4 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Tue, 12 Jun 2018 19:26:05 -0400

python-cassandra-driver (3.7.1-2) unstable; urgency=medium

  * debian/rules
    - remove build and egg-info dirs in clean target, to build the package twice
      in a row; Closes: #825924
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sun, 08 Jan 2017 11:28:48 -0500

python-cassandra-driver (3.7.1-1) unstable; urgency=medium

  * New upstream release
  * debian/rules
    - dont use cython, cassandra-driver is incompatible with 0.25 we have
  * compat level 10

 -- Sandro Tosi <morph@debian.org>  Sat, 17 Dec 2016 13:09:50 -0500

python-cassandra-driver (3.7.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - add dh-python t0 b-d

 -- Sandro Tosi <morph@debian.org>  Sun, 02 Oct 2016 14:23:02 -0400

python-cassandra-driver (3.4.1-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * New upstream release
  * debian/control
    - adjust Vcs-Browser to DPMT standards
    - add libev-dev to b-p needed by cassandra.io.libevwrapper
    - bump Standards-Version to 3.9.8 (no changes needed)
  * debian/copyright
    - extend packaging copyright years
    - update upstream copyright years
  * build arch:any and debug pkgs now that this prj contains extensions

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Sandro Tosi <morph@debian.org>  Sun, 12 Jun 2016 22:58:39 +0100

python-cassandra-driver (2.5.1-1) unstable; urgency=low

  * Initial release (Closes: #771065)

 -- Sandro Tosi <morph@debian.org>  Thu, 25 Jun 2015 15:50:21 -0400
