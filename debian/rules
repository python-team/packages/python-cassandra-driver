#!/usr/bin/make -f

export PYBUILD_NAME=cassandra

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

# In case there's something wrong during cython generation, you may want to enable this
#export CASS_DRIVER_NO_CYTHON=1

# Enable parallel build
export CASS_DRIVER_BUILD_CONCURRENCY=$(shell nproc)

%:
ifeq ($(filter nodoc,$(DEB_BUILD_PROFILES)),)
	dh $@ --with sphinxdoc --buildsystem=pybuild
else
	dh $@ --buildsystem=pybuild
endif

override_dh_auto_clean:
	dh_auto_clean
	rm -rf build/ cassandra_driver.egg-info/
	# Do not delete cmurmur3.c because we don't have proper Python extension
	# so it cannot be regenerated with cython
	find cassandra -maxdepth 1 -type f -name '*.c' ! -name 'cmurmur3.c' -delete

override_dh_auto_build:
	dh_auto_build
ifeq ($(filter nodoc,$(DEB_BUILD_PROFILES)),)
	PYTHONPATH=. http_proxy='127.0.0.1:9' sphinx-build -N -bhtml docs/ build/html
endif

override_dh_auto_test:
ifeq ($(filter nocheck,$(DEB_BUILD_PROFILES)),)
	rm tests/integration -rf
	rm tests/stress_tests/test_multi_inserts.py tests/stress_tests/test_load.py tests/unit/io/test_eventletreactor.py tests/unit/cython/test_utils.py tests/unit/cython/test_bytesio.py
ifeq ($(DEB_HOST_ARCH_ENDIAN),little)
	dh_auto_test -- --test-args "-k 'not test_timer_cancellation'"
else
	-dh_auto_test
endif
endif

override_dh_auto_install:
	dh_auto_install
	rm -rf debian/python*-dbg/usr/lib/python*/dist-packages/cassandra/numpyFlags.h
